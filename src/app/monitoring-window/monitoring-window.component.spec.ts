import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringWindowComponent } from './monitoring-window.component';

describe('MonitoringWindowComponent', () => {
  let component: MonitoringWindowComponent;
  let fixture: ComponentFixture<MonitoringWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
